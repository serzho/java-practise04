package com.practise;

public class FireFighter extends Passenger {

    protected boolean hasAxe;

    public void setAxe(boolean hasAxe) {
        this.hasAxe = hasAxe;
    }

    public boolean hasAxe() {
        return hasAxe;
    }
}
