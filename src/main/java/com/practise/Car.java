package com.practise;

public class Car<T extends Passenger> extends Vehicle<T> {
    public Car(int sits) {
        super(sits);
    }
}
