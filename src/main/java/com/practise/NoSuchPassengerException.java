package com.practise;

public class NoSuchPassengerException extends Exception {
    public NoSuchPassengerException() {
        super("No such passenger in the vehicle!");
    }
}
