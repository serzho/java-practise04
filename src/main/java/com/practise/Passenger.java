package com.practise;

public class Passenger {

    protected int baggageWeight;

    public Passenger() {}

    public int getBaggageWeight() {
        return baggageWeight;
    }

    public void setBaggageWeight(int baggageWeight) {
        this.baggageWeight = baggageWeight;
    }
}
