package com.practise;

public class Policemen extends Passenger {
    protected int ammo;

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public int getAmmo() {
        return ammo;
    }
}
