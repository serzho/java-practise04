package com.practise;

class NoAvailableSitsException extends Exception {
    public NoAvailableSitsException() {
        super("No available sits in the vehicle!");
    }
}

