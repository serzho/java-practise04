package com.practise;

public class Taxi extends Car {
    protected int tax;

    public Taxi(int sits) {
        super(sits);
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }
}
