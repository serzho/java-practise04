package com.practise;

import java.util.*;

public class Vehicle<T extends Passenger> {

    protected int sits;
    protected Set<T> passengers;

    public Vehicle() {
        this(0);
    }

    public Vehicle(int sits) {
        this.sits = sits;
        this.passengers = new HashSet(sits);
    }

    public int getSits() {
        return sits;
    }

    public int getAvailableSits() {
        return getSits() - passengers.size();
    }

    public void sitPassenger(T passenger) throws NoAvailableSitsException {
        if (passengers.size() < getSits())
            passengers.add(passenger);
        else
            throw new NoAvailableSitsException();
    }

    public void landPassenger(T passenger) throws NoSuchPassengerException {
        if (!passengers.remove(passenger))
            throw new NoSuchPassengerException();
    }
}
