package com.practise;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class VehicleTest {

    Vehicle vehicle;

    @Before
    public void up() {
        vehicle = new Vehicle(7);
    }

    @Test
    public void test_on_creating_vehicle_should_have_number_of_sits() {
        assertThat(vehicle.getSits(), equalTo(7));
    }

    @Test
    public void test_if_no_passangers_sitting_should_return_all_number_of_sits() {
        int availableSits = vehicle.getAvailableSits();
        int allSits = vehicle.getSits();
        assertThat(availableSits, equalTo(allSits));
    }

    @Test
    public void test_sit_a_passenger_should_decrement_number_of_available_sits_by_one()
        throws Exception {

        vehicle.sitPassenger(new Passenger());
        int availableSits = vehicle.getAvailableSits();
        assertThat(availableSits, equalTo(6));
    }

    @Test(expected=NoAvailableSitsException.class)
    public void test_sit_a_passenger_to_full_vehicle_should_throw_exception()
        throws NoAvailableSitsException {

        vehicle = new Vehicle(1);
        vehicle.sitPassenger(new Passenger());
        vehicle.sitPassenger(new Passenger());
    }

    @Test
    public void test_landing_a_passenger_should_increment_number_of_available_sits_by_one()
        throws NoSuchPassengerException, NoAvailableSitsException {

        Passenger passenger = new Passenger();
        vehicle.sitPassenger(passenger);
        int sitsWithPassenger = vehicle.getAvailableSits();
        vehicle.landPassenger(passenger);
        int sitsWithoutPassenger = vehicle.getAvailableSits();

        assertThat(sitsWithoutPassenger, is(sitsWithPassenger + 1));
    }

    @Test(expected=NoSuchPassengerException.class)
    public void test_landing_passenger_that_not_in_vehicle_should_throw_exception()
        throws NoSuchPassengerException {
    
        vehicle.landPassenger(new Passenger());
    }

    @Test
    public void test_vehicle_should_sit_only_unique_passengers()
        throws NoAvailableSitsException {

        Passenger passenger = new Passenger();
        vehicle.sitPassenger(passenger);
        vehicle.sitPassenger(passenger);

        assertThat(vehicle.getAvailableSits(), equalTo(6));
    }
}
